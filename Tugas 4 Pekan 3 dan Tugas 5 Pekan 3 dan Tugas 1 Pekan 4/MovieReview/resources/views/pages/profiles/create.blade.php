@extends('layouts.master')

@section('content')
        <div class="ml-3 mt-3 mr-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create a Profile</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/profiles" method="POST">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputNama">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ old('nama', '') }}" id="inputNama" placeholder="Name">
                    @error('nama')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="inputUmur">Umur</label>
                    <input type="number" step="any" min=0 class="form-control" name="umur" value="{{ old('umur', '') }}" id="inputUmur" placeholder="Age">
                    @error('umur')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="inputBio">Bio</label>
                    <input type="text" class="form-control" name="bio" value="{{ old('umur', '') }}" id="inputBio" placeholder="Describe yourself here">
                    @error('bio')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                 </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
        </div>
@endsection