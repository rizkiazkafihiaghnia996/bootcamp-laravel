<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=devive-width, initial-scale=1.0">
    <title>Sign Up</title>
</head>
<body>
    <h1>Buat Account Baru!</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="POST">
        @csrf 
        <label>First Name:</label><br><br>
        <input type="text" name="firstname"><br><br>
        <label>Last Name:</label><br><br>
        <input type="text" name="lastname"><br><br>
        <label>Gender:</label><br><br>
        <input type="radio" name="gender"> Male<br>
        <input type="radio" name="gender"> Female<br>
        <input type="radio" name="gender"> Other<br><br>
        <label>Nationality:</label><br><br>
        <select name="nationality">
            <option value="indonesian">Indonesian</option>
            <option value="american">American</option>
            <option value="british">British</option>
            <option value="mexican">Mexican</option>
            <option value="spanish">Spanish</option>
            <option value="canadian">Canadian</option>
            <option value="japanese">Japanese</option>
            <option value="chinese">Chinese</option>
            <option value="malaysian">Malaysian</option>
            <option value="singapore">Singapore</option>
            <option value="vietnamese">Vietnamese</option>
            <option value="french">French</option>
            <option value="german">German</option>
        </select><br><br>
        <label>Language Spoken:</label><br><br>
        <input type="checkbox">Bahasa Indonesia<br>
        <input type="checkbox">English<br>
        <input type="checkbox">Other<br><br>
        <label>Bio:</label><br><br>
        <textarea name="bio" cols="35" rows="10"></textarea><br>
        <input type="submit" value="Sign Up">
    </form>
</body>
</html>