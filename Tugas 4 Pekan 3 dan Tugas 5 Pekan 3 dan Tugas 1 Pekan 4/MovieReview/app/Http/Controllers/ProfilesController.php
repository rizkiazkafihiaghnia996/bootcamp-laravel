<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class ProfilesController extends Controller
{
    public function create () {
        return view('pages.profiles.create');
    }
    
    public function store (Request $request) {
        //dd($request->all());
        $request -> validate([
            'nama' => "required|max:45",
            'umur' => "required",
            'bio' => "required"
        ]);

        $query = DB::table('profiles')->insert([
            'nama' => $request['nama'],
            'umur' => $request['umur'],
            'bio' => $request['bio'],
            'users_id' => Auth::id()
        ]);
        
        return redirect('/profiles')->with('success', 'Profile was created successfully!');
    }

    public function index() {
        $profiles = DB::table('profiles')->get();
        //dd($profiles->all());
        return view('pages.profiles.index', compact('profiles'));
    }

    public function show($id) {
        $profile = DB::table('profiles')->where('id', $id)->first();
        //dd($profile);
        return view('pages.profiles.show', compact('profile'));
    }

    public function edit($id) {
        $profile = DB::table('profiles')->where('id', $id)->first();
        //dd($profile);
        return view('pages.profiles.edit', compact('profile'));
    }

    public function update($id, Request $request) {
        $request -> validate([
            'nama' => "required|max:45",
            'umur' => "required",
            'bio' => "required"
        ]);

        $query = DB::table('profiles')
                     ->where('id', $id)
                     ->update([
                        'nama' => $request['nama'],
                        'umur' => $request['umur'],
                        'bio' => $request['bio'],
                     ]);
        
        return redirect('/profiles')->with('success', 'Update saved!');
    }

    public function destroy($id) {
        $query = DB::table('profiles')->where('id', $id)->delete();
        return redirect('/profiles')->with('success', 'Profile was successfully deleted!');
    }
}

