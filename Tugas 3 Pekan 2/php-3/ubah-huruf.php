<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Ubah Huruf</title>
</head>
<body>
    <h1>Soal 2: Ubah Huruf</h1>
    <?php
    function ubah_huruf($string) {
        //kode di sini
        $alphabet = "abcdefghijklmnopqrstuvwxyz";
        $output = "";
        for ($i = 0; $i < strlen($string);  $i++) {
            $position = strpos($alphabet, $string[$i]);
            $output .= substr($alphabet, $position + 1, 1);
        }
        return $output. "<br>";
    }
        
    // TEST CASES
    echo ubah_huruf('wow'); // xpx
    echo ubah_huruf('developer'); // efwfmpqfs
    echo ubah_huruf('laravel'); // mbsbwfm
    echo ubah_huruf('keren'); // lfsfo
    echo ubah_huruf('semangat'); // tfnbohbu
?>
</body>
</html>