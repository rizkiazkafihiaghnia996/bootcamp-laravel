<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Cast;

class CastController extends Controller
{
        public function __construct() {
            $this->middleware('auth')->except(['index', 'show']);
        }
        public function create () {
            return view('pages.casts.create');
        }
        
        public function store (Request $request) {
            //dd($request->all());
            $request -> validate([
                'nama' => "required|max:45",
                'umur' => "required",
            ]);
    
            /*$query = DB::table('casts')->insert([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
            ]);*/
                $cast = new Cast;
                $cast->nama = $request['nama'];
                $cast->umur = $request['umur'];
            
                $cast->save();
            
            return redirect('/casts')->with('success', 'Cast profile was created successfully!');
        }
    
        public function index() {
            //$casts = DB::table('casts')->get();
            //dd($casts->all());
            $casts = Cast::all();
            return view('pages.casts.index', compact('casts'));
        }
    
        public function show($id) {
            //$cast = DB::table('casts')->where('id', $id)->first();
            //dd($cast);
            $cast = Cast::find($id);
            return view('pages.casts.show', compact('cast'));
        }
    
        public function edit($id) {
            //$cast = DB::table('casts')->where('id', $id)->first();
            //dd($cast);
            $cast = Cast::find($id);
            return view('pages.casts.edit', compact('cast'));
        }
    
        public function update($id, Request $request) {
            $request -> validate([
                'nama' => "required|max:45",
                'umur' => "required",
            ]);
                
            $update = Cast::where('id', $id)->update([
                'nama' => $request['nama'],
                'umur' => $request['umur'],
            ]);
            /*$query = DB::table('casts')
                         ->where('id', $id)
                         ->update([
                            'nama' => $request['nama'],
                            'umur' => $request['umur'],
                         ]);*/
            
            return redirect('/casts')->with('success', 'Update saved!');
        }
    
        public function destroy($id) {
            //$query = DB::table('casts')->where('id', $id)->delete();
            $destroy = Cast::destroy($id);
             return redirect('/casts')->with('success', 'Cast profile was successfully deleted!');
        }
}
