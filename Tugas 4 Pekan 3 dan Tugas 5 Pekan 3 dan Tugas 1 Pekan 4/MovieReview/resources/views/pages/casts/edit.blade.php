@extends('layouts.master')

@section('content')
        <div class="ml-3 mt-3 mr-3">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Cast Profile {{$cast->id}}</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form action="/casts/{{$cast->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="inputNama">Nama</label>
                    <input type="text" class="form-control" name="nama" value="{{ old('nama', $cast->nama) }}" id="inputNama" placeholder="Name">
                    @error('nama')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="inputUmur">Umur</label>
                    <input type="number" step="any" min=0 class="form-control" name="umur" value="{{ old('umur', $cast->umur) }}" id="inputUmur" placeholder="Age">
                    @error('umur')
                      <div class='alert alert-danger'>{{ $message }}</div>
                    @enderror
                  </div>
                </div>
                <!-- /.card-body -->
                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Edit</button>
                </div>
              </form>
            </div>
        </div>
@endsection