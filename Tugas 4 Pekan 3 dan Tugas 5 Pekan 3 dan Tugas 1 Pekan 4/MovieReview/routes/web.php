<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/master', function () {
    return view('layouts.master');
});

/*Route::get('profiles/create', 'ProfilesController@create');
Route::post('profiles', 'ProfilesController@store');
Route::get('profiles', 'ProfilesController@index');
Route::get('profiles/{id}', 'ProfilesController@show');
Route::get('profiles/{id}/edit', 'ProfilesController@edit');
Route::put('profiles/{id}', 'ProfilesController@update');
Route::delete('profiles/{id}', 'ProfilesController@destroy');

Route::get('casts/create', 'CastController@create');
Route::post('casts', 'CastController@store');
Route::get('casts', 'CastController@index')->name('casts.index');
Route::get('casts/{id}', 'CastController@show');
Route::get('casts/{id}/edit', 'CastController@edit');
Route::put('casts/{id}', 'CastController@update');
Route::delete('casts/{id}', 'CastController@destroy');
*/

//Route::resource('pemeran', 'PemeranController');

Route::resource('profiles', 'ProfilesController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('casts', 'CastController');

