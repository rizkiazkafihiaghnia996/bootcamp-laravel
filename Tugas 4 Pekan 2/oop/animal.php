<?php
    class Animal {
        public function __construct($name) {
            $this->name = $name;
        }
        public $legs = 4;
        public $cold_blooded = "No";
    }
?>