@extends('layouts.master')

@section('content')
    <div class='ml-3 mt-3 mr-3'>
    <div class="card">
              <div class="card-header">
                <h3 class="card-title">Cast Profile Table</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                  @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                  @endif
                <div class="mb-3">
                  <a class="btn btn-outline-secondary my-2 my-sm-0" href="{{route('casts.create')}}">Create a New Cast Profile</a>
                </div>
                <table class="table table-bordered">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th style="width: 250px">Nama</th>
                      <th style="width: 10px">Umur</th>
                      <th style="width: 110px">Link</th>
                    </tr>
                  </thead>
                  <tbody>
                    @forelse($casts as $key => $cast)
                        <tr>
                            <td>{{ $key + 1 }}</td>
                            <td>{{ $cast -> nama }}</td>
                            <td>{{ $cast -> umur }}</td>
                            <td style="display: flex">
                                <a href="{{route('casts.show', ['cast' => $cast->id])}}" class="btn btn-outline-secondary btn-sm mr-1">Go</a>
                                <a href="{{route('casts.edit', ['cast' => $cast->id])}}" class="btn btn-outline-primary btn-sm mr-1">Edit</a>
                                <form action="{{route('casts.destroy', ['cast' => $cast->id])}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" value='Delete' class="btn btn-outline-danger btn-sm">
                                </form>
                            </td>
                        </tr>
                        @empty
                            <tr>
                                <td colspan="4" align="center">No cast profile found</td>
                            </tr>
                    @endforelse
                  </tbody>
                </table>
              </div>
              <!-- /.card-body 
              <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                  <li class="page-item"><a class="page-link" href="#">«</a></li>
                  <li class="page-item"><a class="page-link" href="#">1</a></li>
                  <li class="page-item"><a class="page-link" href="#">2</a></li>
                  <li class="page-item"><a class="page-link" href="#">3</a></li>
                  <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
              </div>
              -->
            </div>
    </div>
@endsection