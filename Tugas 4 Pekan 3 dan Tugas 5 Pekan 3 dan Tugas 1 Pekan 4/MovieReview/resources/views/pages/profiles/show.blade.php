@extends('layouts.master')

@section('content')
    <div class="mt-3 ml-3">
        <h4> {{ $profile->nama }} </h4>
        <h4> {{ $profile->umur }} </h4>
        <p> {{ $profile->bio }} </p>
    </div>
@endsection